$(document).ready(function() {
    Module.addHeader();
    Module.timeSpent();
    Module.clock();
});

var Module = (function () {

    function _alertMe(i) {
        if (i == 5) {
            alert('five');
        } else
            alert(i);
    }

    function _setTwoDigits(time) {
        return (time < 10 ? '0' : '') + time;
    }

    function loadList() {
        $(".THELIST").empty();
        for (i = 0; i < 10; i++) {
            var newElem = $("<li><a href='#'>" + i + "</a></li>");
            newElem.click(function() {
                _alertMe(this.innerText);
            });
            $(".THELIST").append(newElem);
        }
    }

    function clock() {
        var date = new Date();
        var hours = _setTwoDigits(date.getHours());
        var minutes = _setTwoDigits(date.getMinutes());
        var seconds = _setTwoDigits(date.getSeconds());
        $("#Clock").text(hours + ":" + minutes + ":" + seconds);
    }

    function timeSpent() {
        function _tooLong() {
            alert("You have now been on the page for half a minute!");
        }
        setTimeout(_tooLong, 30000);
    }

    function addHeader() {
        $('#header').append('<h1>The fantastic javascript example</h1>');
    }

    return {
        addHeader: addHeader,
        loadList: loadList,
        timeSpent: timeSpent,
        clock: clock
    }

})();

